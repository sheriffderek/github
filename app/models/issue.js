import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { belongsTo } from 'ember-data/relationships';

export default Model.extend({
  repo: belongsTo('repo'),

  url: attr(),
  number: attr(),
  title: attr(),
  user: attr(),
  labels: attr(),
  state: attr(),
  locked: attr(),
  assignee: attr(),
  milestone: attr(),
  comments: attr(),
  createdAt: attr(),
  updatedAt: attr(),
  closedAt: attr(),
  pullRequest: attr(),
  body: attr(),
  closedBy: attr(),
  githubId: attr()
});
